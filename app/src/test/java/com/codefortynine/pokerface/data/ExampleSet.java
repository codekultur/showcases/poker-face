package com.codefortynine.pokerface.data;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardSuite;
import com.codefortynine.pokerface.models.CardValue;

import java.util.List;

public class ExampleSet {
    public static List<Card> straightFlush = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.HEARTS, CardValue.THREE),
        new Card(CardSuite.HEARTS, CardValue.FOUR),
        new Card(CardSuite.HEARTS, CardValue.FIVE),
        new Card(CardSuite.HEARTS, CardValue.SIX)
    );

    public static List<Card> fourOfAKind = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.TWO),
        new Card(CardSuite.SPADES, CardValue.TWO),
        new Card(CardSuite.HEARTS, CardValue.SIX)
    );

    public static List<Card> fullHouse = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.KING),
        new Card(CardSuite.SPADES, CardValue.KING),
        new Card(CardSuite.HEARTS, CardValue.KING)
    );

    public static List<Card> flush = List.of(
        new Card(CardSuite.DIAMONDS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.SIX),
        new Card(CardSuite.DIAMONDS, CardValue.KING),
        new Card(CardSuite.DIAMONDS, CardValue.FOUR),
        new Card(CardSuite.DIAMONDS, CardValue.SEVEN)
    );

    public static List<Card> straight = List.of(
        new Card(CardSuite.CLUBS, CardValue.THREE),
        new Card(CardSuite.DIAMONDS, CardValue.SIX),
        new Card(CardSuite.DIAMONDS, CardValue.FOUR),
        new Card(CardSuite.SPADES, CardValue.FIVE),
        new Card(CardSuite.SPADES, CardValue.SEVEN)
    );

    public static List<Card> threeOfAKind = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.FIVE),
        new Card(CardSuite.SPADES, CardValue.TWO),
        new Card(CardSuite.HEARTS, CardValue.SIX)
    );

    public static List<Card> twoPairs = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.FIVE),
        new Card(CardSuite.SPADES, CardValue.SIX),
        new Card(CardSuite.HEARTS, CardValue.SIX)
    );

    public static List<Card> pair = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.TWO),
        new Card(CardSuite.DIAMONDS, CardValue.FIVE),
        new Card(CardSuite.SPADES, CardValue.SIX),
        new Card(CardSuite.HEARTS, CardValue.KING)
    );

    public static List<Card> highCard = List.of(
        new Card(CardSuite.HEARTS, CardValue.TWO),
        new Card(CardSuite.CLUBS, CardValue.FOUR),
        new Card(CardSuite.DIAMONDS, CardValue.FIVE),
        new Card(CardSuite.SPADES, CardValue.NINE),
        new Card(CardSuite.HEARTS, CardValue.KING)
    );
}
