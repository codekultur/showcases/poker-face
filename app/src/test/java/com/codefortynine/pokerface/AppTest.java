package com.codefortynine.pokerface;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import org.junit.jupiter.api.Test;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardSuite;
import com.codefortynine.pokerface.models.CardValue;

class AppTest {
    @Test
    void playHandsWithHandOneWinning() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.SIX)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.NINE),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.TWO)
        );

        assertEquals("Hand 1 has won!", new App().playHands(handOne, handTwo));
    }

    @Test
    void playHandsWithHandTwoWinning() {
        List<Card> handOne = List.of(
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.SPADES, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.SIX)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.SPADES, CardValue.ACE),
            new Card(CardSuite.CLUBS, CardValue.ACE)
        );

        assertEquals("Hand 2 has won!", new App().playHands(handOne, handTwo));
    }

    @Test
    void playHandsWithNoHandWinning() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TEN),
            new Card(CardSuite.HEARTS, CardValue.JACK),
            new Card(CardSuite.HEARTS, CardValue.QUEEN),
            new Card(CardSuite.HEARTS, CardValue.KING),
            new Card(CardSuite.HEARTS, CardValue.ACE)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.JACK),
            new Card(CardSuite.CLUBS, CardValue.QUEEN),
            new Card(CardSuite.CLUBS, CardValue.KING),
            new Card(CardSuite.CLUBS, CardValue.ACE)
        );

        assertEquals("Both hands have won.", new App().playHands(handOne, handTwo));
    }
}
