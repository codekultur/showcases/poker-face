package com.codefortynine.pokerface;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;

public class UtilsTest {
    @Test
    void areNumbersConsecutiveReturnsTrue() {
        List<Integer> numbers = List.of(2, 3, 4, 5, 6);

        assertTrue(Utils.areNumbersConsecutive(numbers));
    }

    @Test
    void areNumbersConsecutiveReturnsFalse() {
        List<Integer> numbers = List.of(2, 3, 5, 6);

        assertFalse(Utils.areNumbersConsecutive(numbers));
    }

    @Test
    void getRedundantNumbers() {
        List<Integer> numbers = List.of(2, 2, 4, 6, 2);

        assertEquals(List.of(2), Utils.getRedundantNumbers(numbers));

        numbers = List.of(2, 2, 4, 6, 2, 6);

        assertEquals(List.of(6, 2), Utils.getRedundantNumbers(numbers));
    }

    @Test
    void getUniqueNumbers() {
        List<Integer> numbers = List.of(2, 2, 4, 5, 6, 2, 5);

        assertEquals(List.of(6, 4), Utils.getUniqueNumbers(numbers));
    }
}
