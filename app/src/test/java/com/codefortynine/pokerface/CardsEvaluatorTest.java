package com.codefortynine.pokerface;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.codefortynine.pokerface.data.ExampleSet;
import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardSuite;
import com.codefortynine.pokerface.models.CardValue;

public class CardsEvaluatorTest {
    CardsEvaluator evaluator;

   @BeforeEach
   public void init() {
        evaluator = new CardsEvaluator();
    }

    @ParameterizedTest
    @MethodSource("generateTestData")
    void getWinningHand(List<Card> handOne, List<Card> handTwo, int expectedResult) {
        assertEquals(expectedResult, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSamePairs() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.SIX)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.SPADES, CardValue.TWO)
        );

        assertEquals(-1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameTwoPairs() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.TEN),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.SPADES, CardValue.FIVE)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.DIAMONDS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.SPADES, CardValue.FOUR),
            new Card(CardSuite.SPADES, CardValue.TWO)
        );

        assertEquals(1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameThreeOfAKind() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.SPADES, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.SIX)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.SPADES, CardValue.TWO)
        );

        assertEquals(-1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameStraight() {
        List<Card> handOne = List.of(
            new Card(CardSuite.CLUBS, CardValue.SEVEN),
            new Card(CardSuite.HEARTS, CardValue.NINE),
            new Card(CardSuite.SPADES, CardValue.EIGHT),
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.JACK)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.DIAMONDS, CardValue.FIVE),
            new Card(CardSuite.SPADES, CardValue.SIX)
        );

        assertEquals(1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameFlush() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.TEN),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.KING)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.CLUBS, CardValue.THREE),
            new Card(CardSuite.CLUBS, CardValue.ACE),
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.FIVE)
        );

        assertEquals(-1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameFullHouse() {
        List<Card> handOne = List.of(
            new Card(CardSuite.DIAMONDS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.SPADES, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.THREE)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.HEARTS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.TWO)
        );

        assertEquals(-1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameFourOfAKind() {
        List<Card> handOne = List.of(
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.SPADES, CardValue.TWO),
            new Card(CardSuite.CLUBS, CardValue.TWO)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.CLUBS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.DIAMONDS, CardValue.TWO),
            new Card(CardSuite.SPADES, CardValue.TWO)
        );

        assertEquals(0, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameStraightFlush() {
        List<Card> handOne = List.of(
            new Card(CardSuite.CLUBS, CardValue.SEVEN),
            new Card(CardSuite.CLUBS, CardValue.NINE),
            new Card(CardSuite.CLUBS, CardValue.EIGHT),
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.JACK)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.HEARTS, CardValue.THREE),
            new Card(CardSuite.HEARTS, CardValue.TWO),
            new Card(CardSuite.HEARTS, CardValue.FOUR),
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.SIX)
        );

        assertEquals(1, evaluator.compare(handOne, handTwo));
    }

    @Test
    void getWinningHandForSameRoyalFlush() {
        List<Card> handOne = List.of(
            new Card(CardSuite.CLUBS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.JACK),
            new Card(CardSuite.CLUBS, CardValue.QUEEN),
            new Card(CardSuite.CLUBS, CardValue.KING),
            new Card(CardSuite.CLUBS, CardValue.ACE)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.HEARTS, CardValue.TEN),
            new Card(CardSuite.HEARTS, CardValue.JACK),
            new Card(CardSuite.HEARTS, CardValue.QUEEN),
            new Card(CardSuite.HEARTS, CardValue.KING),
            new Card(CardSuite.HEARTS, CardValue.ACE)
        );

        assertEquals(0, evaluator.compare(handOne, handTwo));
    }

    private static Stream<Arguments> generateTestData() {
        return Stream.of(
            Arguments.of(ExampleSet.twoPairs, ExampleSet.pair, 1),
            Arguments.of(ExampleSet.twoPairs, ExampleSet.straight, -1),
            Arguments.of(ExampleSet.fullHouse, ExampleSet.straight, 1),
            Arguments.of(ExampleSet.fullHouse, ExampleSet.fourOfAKind, -1),
            Arguments.of(ExampleSet.straightFlush, ExampleSet.fourOfAKind, 1)
        );
    }
}
