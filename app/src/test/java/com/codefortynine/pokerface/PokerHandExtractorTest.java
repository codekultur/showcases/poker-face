package com.codefortynine.pokerface;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.stream.Stream;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.Arguments;
import org.junit.jupiter.params.provider.MethodSource;

import com.codefortynine.pokerface.data.ExampleSet;
import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.PokerHand;

public class PokerHandExtractorTest {
    @ParameterizedTest
    @MethodSource("generateTestData")
    void extractHighestPokerHand(List<Card> cards, PokerHand expectedResult) {
        PokerHand extractedPokerHand = PokerHandExtractor.extractHighestPokerHand(cards);

        assertEquals(expectedResult, extractedPokerHand);
    }

    private static Stream<Arguments> generateTestData() {
        return Stream.of(
            Arguments.of(ExampleSet.highCard, PokerHand.HIGH_CARD),
            Arguments.of(ExampleSet.pair, PokerHand.PAIR),
            Arguments.of(ExampleSet.twoPairs, PokerHand.TWO_PAIRS),
            Arguments.of(ExampleSet.threeOfAKind, PokerHand.THREE_OF_A_KIND),
            Arguments.of(ExampleSet.fullHouse, PokerHand.FULL_HOUSE),
            Arguments.of(ExampleSet.flush, PokerHand.FLUSH),
            Arguments.of(ExampleSet.straight, PokerHand.STRAIGHT),
            Arguments.of(ExampleSet.fourOfAKind, PokerHand.FOUR_OF_A_KIND),
            Arguments.of(ExampleSet.straightFlush, PokerHand.STRAIGHT_FLUSH)
        );
    }
}
