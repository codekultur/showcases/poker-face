package com.codefortynine.pokerface;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.List;
import org.junit.jupiter.api.Test;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardSuite;
import com.codefortynine.pokerface.models.CardValue;

public class CardsValidatorTest {
    @Test
    void invalidatesASetWithDuplicateCards() {
        List<Card> invalidSet = List.of(
            new Card(CardSuite.CLUBS, CardValue.FIVE),
            new Card(CardSuite.CLUBS, CardValue.FIVE)
        );

        assertFalse(CardsValidator.isSetOfCardsValid(invalidSet), "Validator should invalidate the given set");
    }

    @Test
    void validatesASetWithUniqueCards() {
        List<Card> validSet = List.of(
            new Card(CardSuite.CLUBS, CardValue.THREE),
            new Card(CardSuite.CLUBS, CardValue.FIVE)
        );

        assertTrue(CardsValidator.isSetOfCardsValid(validSet), "Validator should validate the given set");
    }
}
