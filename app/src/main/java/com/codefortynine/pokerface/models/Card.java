package com.codefortynine.pokerface.models;

public class Card {
    private final CardSuite suite;
    private final CardValue value;

    public Card(CardSuite suite, CardValue value) {
        this.suite = suite;
        this.value = value;
    }

    public CardSuite getSuite() {
        return this.suite;
    }

    public CardValue getValue() {
        return this.value;
    }

    @Override
    public boolean equals(Object object) {
        if (object == this) {
            return true;
        }

        if (!(object instanceof Card anotherCard)) {
            return false;
        }

        return this.suite == anotherCard.suite &&
            this.value == anotherCard.value;
    }

    @Override
    public final int hashCode() {
        return suite.hashCode() + value.hashCode();
    }
}
