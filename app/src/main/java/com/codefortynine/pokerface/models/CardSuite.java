package com.codefortynine.pokerface.models;

public enum CardSuite {
    HEARTS,
    DIAMONDS,
    CLUBS,
    SPADES
}
