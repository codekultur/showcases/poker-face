package com.codefortynine.pokerface;

import java.util.List;

import com.codefortynine.pokerface.models.Card;

public class CardsValidator {
    /**
     * Checks the set of cards for duplicates and returns false if
     * there are any. If there are no duplicates, it returns true.
     */
    public static boolean isSetOfCardsValid(List<Card> cards) {
        return cards.stream()
            .reduce(
                true,
                (accumulator, card) -> accumulator && cards.stream().filter(c -> c.equals(card)).toList().size() == 1,
                Boolean::logicalAnd
            );
    }
}
