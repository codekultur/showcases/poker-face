package com.codefortynine.pokerface;

import java.util.List;
import java.util.stream.Stream;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardSuite;
import com.codefortynine.pokerface.models.CardValue;

public class App {
    public String playHands(List<Card> handOne, List<Card> handTwo) {
        CardsEvaluator evaluator = new CardsEvaluator();
        int result = evaluator.compare(handOne, handTwo);

        switch (result) {
            case 1:
                return "Hand 1 has won!";
            case -1:
                return "Hand 2 has won!";
            default:
                return "Both hands have won.";
        }
    }

    public static void main(String[] args) {
        List<Card> handOne = List.of(
            new Card(CardSuite.CLUBS, CardValue.FIVE),
            new Card(CardSuite.HEARTS, CardValue.NINE),
            new Card(CardSuite.HEARTS, CardValue.ACE),
            new Card(CardSuite.SPADES, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.TWO)
        );
        List<Card> handTwo = List.of(
            new Card(CardSuite.HEARTS, CardValue.FIVE),
            new Card(CardSuite.CLUBS, CardValue.NINE),
            new Card(CardSuite.HEARTS, CardValue.KING),
            new Card(CardSuite.DIAMONDS, CardValue.TEN),
            new Card(CardSuite.CLUBS, CardValue.FOUR)
        );

        List<Card> allCards = Stream.concat(handOne.stream(), handTwo.stream()).toList();
        boolean isSetValid = CardsValidator.isSetOfCardsValid(allCards);

        if (isSetValid) {
            String result = new App().playHands(handOne, handTwo);
    
            System.out.println(result);
        } else {
            System.out.println("The set of cards is not a valid poker deck. Are there duplicate cards in the two hands?");
        }
    }
}
