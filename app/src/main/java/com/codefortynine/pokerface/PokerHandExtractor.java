package com.codefortynine.pokerface;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardValue;
import com.codefortynine.pokerface.models.PokerHand;

public class PokerHandExtractor {
    public static PokerHand extractHighestPokerHand(List<Card> cards) {
        if (isStraightFlush(cards)) {
            return PokerHand.STRAIGHT_FLUSH;
        } else if (isFourOfAKind(cards)) {
            return PokerHand.FOUR_OF_A_KIND;
        } else if (isFullHouse(cards)) {
            return PokerHand.FULL_HOUSE;
        } else if (isFlush(cards)) {
            return PokerHand.FLUSH;
        } else if (isStraight(cards)) {
            return PokerHand.STRAIGHT;
        } else if (isThreeOfAKind(cards)) {
            return PokerHand.THREE_OF_A_KIND;
        } else if (isTwoPairs(cards)) {
            return PokerHand.TWO_PAIRS;
        } else if (isPair(cards)) {
            return PokerHand.PAIR;
        }

        return PokerHand.HIGH_CARD;
    }

    private static boolean isStraightFlush(List<Card> cards) {
        return isFlush(cards) && isStraight(cards);
    }

    private static boolean isFourOfAKind(List<Card> cards) {
        return getNumberOfCardsPerCardValue(cards).values().stream().anyMatch(v -> v.equals(4L));
    }

    private static boolean isFlush(List<Card> cards) {
        return cards.stream().allMatch(c -> c.getSuite() == cards.getFirst().getSuite());
    }

    private static boolean isFullHouse(List<Card> cards) {        
        return getNumberOfCardsPerCardValue(cards).values().containsAll(List.of(2L, 3L));
    }

    private static boolean isStraight(List<Card> cards) {
        List<Integer> cardValues = cards.stream().map(Card::getValue).map(CardValue::value).toList();
        return Utils.areNumbersConsecutive(cardValues);
    }

    private static boolean isThreeOfAKind(List<Card> cards) {
        return getNumberOfCardsPerCardValue(cards).values().stream().anyMatch(v -> v.equals(3L));
    }

    private static boolean isTwoPairs(List<Card> cards) {
        long numberOfPairs = getNumberOfCardsPerCardValue(cards).values().stream().filter(v -> v.equals(2L)).count();

        return numberOfPairs == 2;
    }

    private static boolean isPair(List<Card> cards) {
        return getNumberOfCardsPerCardValue(cards).values().stream().anyMatch(v -> v.equals(2L));
    }

    private static Map<CardValue, Long> getNumberOfCardsPerCardValue(List<Card> cards) {
        return cards.stream().collect(Collectors.groupingBy(Card::getValue, Collectors.counting()));
    }
}
