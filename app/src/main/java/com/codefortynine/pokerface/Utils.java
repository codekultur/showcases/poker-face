package com.codefortynine.pokerface;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class Utils {
    /**
     * Returns true if the given numbers are consecutive otherwise it
     * returns false.
     */
    public static boolean areNumbersConsecutive(List<Integer> numbers) {
        List<Integer> sortedNumbers = numbers.stream().sorted().toList();

        for (int i = 1; i < sortedNumbers.size(); i++) {
            if (sortedNumbers.get(i) != sortedNumbers.get(i - 1) + 1) {
                return false;
            }
        }
        
        return true;
    }

    /**
     * Returns a list of those numbers that occur multiple times in the given
     * list of numbers.
     */
    public static List<Integer> getRedundantNumbers(List<Integer> numbers) {
        Set<Integer> resultSet = new HashSet<>();

        for (Integer i : numbers) {
            if (numbers.stream().filter(n -> n.equals(i)).toList().size() > 1) {
                resultSet.add(i);
            }
        }

        return new ArrayList<>(resultSet).stream().sorted().toList().reversed();
    }

    /**
     * Returns a list of those numbers that are unique in the given list of numbers.
     */
    public static List<Integer> getUniqueNumbers(List<Integer> numbers) {
        List<Integer> resultList = new ArrayList<>();

        for (Integer i : numbers) {
            if (numbers.stream().filter(n -> n.equals(i)).toList().size() == 1) {
                resultList.add(i);
            }
        }

        return resultList.stream().sorted().toList().reversed();
    }
}
