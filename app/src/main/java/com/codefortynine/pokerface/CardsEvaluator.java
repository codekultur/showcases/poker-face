package com.codefortynine.pokerface;

import java.util.Comparator;
import java.util.List;
import java.util.stream.Stream;

import com.codefortynine.pokerface.models.Card;
import com.codefortynine.pokerface.models.CardValue;
import com.codefortynine.pokerface.models.PokerHand;

public class CardsEvaluator implements Comparator<List<Card>> {
    @Override
    public int compare(List<Card> handOne, List<Card> handTwo) {
        PokerHand pokerHandOne = PokerHandExtractor.extractHighestPokerHand(handOne);
        PokerHand pokerHandTwo = PokerHandExtractor.extractHighestPokerHand(handTwo);

        int result = Integer.compare(pokerHandOne.value(), pokerHandTwo.value());

        if (result != 0) {
            return result;
        }

        // Poker hands are the same so further evaluations are required

        switch (pokerHandOne) {
            case PokerHand.PAIR:
            case PokerHand.TWO_PAIRS:
            case PokerHand.THREE_OF_A_KIND:
                return getWinningHandForMultiplesOfAKind(handOne, handTwo);
            case PokerHand.FOUR_OF_A_KIND:
                return getWinningHandForFourOfAKind(handOne, handTwo);
            case PokerHand.STRAIGHT:
            case PokerHand.STRAIGHT_FLUSH:
                return getWinningHandBasedOnHighestCardValue(handOne, handTwo);
            case PokerHand.FULL_HOUSE:
                return getWinningHandForFullHouse(handOne, handTwo);
            default:
                return getWinningHandForHighCard(handOne, handTwo);
        }
    }

    private static int getWinningHandBasedOnHighestCardValue(List<Card> handOne, List<Card> handTwo) {
        List<Integer> handOneValues = handOne.stream().map(Card::getValue).map(CardValue::value).sorted().toList();
        List<Integer> handTwoValues = handTwo.stream().map(Card::getValue).map(CardValue::value).sorted().toList();
        
        return Integer.compare(handOneValues.getLast(), handTwoValues.getLast());
    }

    private static int getWinningHandForFullHouse(List<Card> handOne, List<Card> handTwo) {
        List<Integer> handOneValues = handOne.stream().map(Card::getValue).map(CardValue::value).toList();
        List<Integer> handTwoValues = handTwo.stream().map(Card::getValue).map(CardValue::value).toList();

        List<Integer> redundantCardsInHandOne = Utils.getRedundantNumbers(handOneValues);
        List<Integer> redundantCardsInHandTwo = Utils.getRedundantNumbers(handTwoValues);

        Integer dominantNumberInHandOne = handOneValues.stream().filter(v -> v.equals(redundantCardsInHandOne.getFirst())).toList().size() == 3
            ? redundantCardsInHandOne.getFirst()
            : redundantCardsInHandOne.getLast();

        Integer dominantNumberInHandTwo = handTwoValues.stream().filter(v -> v.equals(redundantCardsInHandTwo.getFirst())).toList().size() == 3
            ? redundantCardsInHandTwo.getFirst()
            : redundantCardsInHandTwo.getLast();

        return Integer.compare(dominantNumberInHandOne, dominantNumberInHandTwo);
    }

    private static int getWinningHandForHighCard(List<Card> handOne, List<Card> handTwo) {
        List<Integer> handOneValues = handOne.stream().map(Card::getValue).map(CardValue::value).sorted().toList().reversed();
        List<Integer> handTwoValues = handTwo.stream().map(Card::getValue).map(CardValue::value).sorted().toList().reversed();

        for (int i = 0; i < handOneValues.size(); i++) {
            int result = Integer.compare(handOneValues.get(i), handTwoValues.get(i));
            
            if (result != 0) {
                return result;
            }
        }

        return 0;
    }

    private static int getWinningHandForFourOfAKind(List<Card> handOne, List<Card> handTwo) {
        List<Integer> handOneValues = handOne.stream().map(Card::getValue).map(CardValue::value).toList();
        List<Integer> handTwoValues = handTwo.stream().map(Card::getValue).map(CardValue::value).toList();

        List<Integer> redundantCardsInHandOne = Utils.getRedundantNumbers(handOneValues);
        List<Integer> redundantCardsInHandTwo = Utils.getRedundantNumbers(handTwoValues);

        return Integer.compare(redundantCardsInHandOne.getFirst(), redundantCardsInHandTwo.getFirst());
    }

    private static int getWinningHandForMultiplesOfAKind(List<Card> handOne, List<Card> handTwo) {
        List<Integer> handOneValues = handOne.stream().map(Card::getValue).map(CardValue::value).sorted().toList().reversed();
        List<Integer> handTwoValues = handTwo.stream().map(Card::getValue).map(CardValue::value).sorted().toList().reversed();

        List<Integer> redundantCardsInHandOne = Utils.getRedundantNumbers(handOneValues);
        List<Integer> redundantCardsInHandTwo = Utils.getRedundantNumbers(handTwoValues);
        
        List<Integer> orderedCardsHandOne = Stream.concat(redundantCardsInHandOne.stream(), Utils.getUniqueNumbers(handOneValues).stream()).toList();
        List<Integer> orderedCardsHandTwo = Stream.concat(redundantCardsInHandTwo.stream(), Utils.getUniqueNumbers(handTwoValues).stream()).toList();

        for (int i = 0; i < orderedCardsHandOne.size(); i++) {
            int result = Integer.compare(orderedCardsHandOne.get(i), orderedCardsHandTwo.get(i));
            
            if (result != 0) {
                return result;
            }
        }
        
        return 0;
    }
}
